 /* global Handlebars, moment, Backendless, userLoggedOut, tinymce, Materialize */

$(function () {
    var APPLICATION_ID = "41090CE2-4E0F-1A85-FFF9-DFA60AC0F500",
            SECRET_KEY = "E2C3389C-9A60-D0AE-FFF8-E0B7D3A80A00",
            VERSION = "v1";

  
   Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    if (Backendless.UserService.isValidLogin()) {
      userLoggedIn(Backendless.LocalCache.get("current-user-id")); 
    } else {
        var loginScript = $("#login-template").html();
        var loginTemplate = Handlebars.compile(loginScript);

        $('.main-container').html(loginTemplate);
    }
    $(document).on('submit', '.form-signin', function (event) {
        event.preventDefault();

        var data = $(this).serializeArray(),
                name = data[0].value,
                password = data[1].value;

        Backendless.UserService.login(name, password, true, new Backendless.Async(userLoggedIn, gotError));
    });

    $(document).on('click', '.add-task', function () {

        var addtaskScript = $("#add-task-template").html();
        var addtaskTemplate = Handlebars.compile(addtaskScript);

        $('.main-container').html(addtaskTemplate);
        tinymce.init({selector: 'textarea',
                    plugins: [
                        "advlist autolink lists link image charmap print preview anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media table contextmenu paste"
                    ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"});

    });
    $(document).on('submit', '.form-add-task', function (event) {
        event.preventDefault();
        var data = $(this).serializeArray(),
                content = data[0].value;

        if (content === "")
        {
            Materialize.toast('Cant leave Task empty', 2000);
        }
        else {
            Materialize.toast('POSTED', 2000);
            var dataStore = Backendless.Persistence.of(Posts);
            console.log(Backendless.UserService.getCurrentUser());
            var postObject = new Posts({
                content: content,
                complete: false,
                authorName: Backendless.UserService.getCurrentUser().name
            });
            dataStore.save(postObject);
            this.content.value = "";

        }
    });
    $(document).on('click', '.logout', function () {
        Backendless.UserService.logout(new Backendless.Async(userLoggedout, gotError));
        var loginScript = $("#login-template").html();
        var loginTemplate = Handlebars.compile(loginScript);

        $('.main-container').html(loginTemplate);
    });
});

function userLoggedout() {
    console.log("logged out");
}
function Posts(args) {
    args = args || {};
    this.content = args.content || "";
    this.authorName = args.authorName || "";
    this.complete = args.complete || "";
}

function userLoggedIn(user) {
    console.log("logged in");
    var userData;
    if (typeof user === "string") {
        userData = Backendless.Data.of(Backendless.User).findById(user);
    } else {
        userData = user;
    }
    var welcomeScript = $('#welcome-template').html();
    var welcomeTemplate = Handlebars.compile(welcomeScript);
    var welcomeHTML = welcomeTemplate(userData);

    $('.main-container').html(welcomeHTML);
    
}
function gotError() {
    Materialize.toast('Somethings wrong', 2000);
}