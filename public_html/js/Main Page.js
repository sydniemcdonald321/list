/* global Backendless, backendless, post, Handlebars, Materialize */

$(function () {
    var APPLICATION_ID = "41090CE2-4E0F-1A85-FFF9-DFA60AC0F500",
            SECRET_KEY = "E2C3389C-9A60-D0AE-FFF8-E0B7D3A80A00",
            VERSION = "v1";
Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);

    if (Backendless.UserService.isValidLogin()) {
        userLoggedIn(Backendless.LocalCache.get("current-user-id"));
    }
    var userData;
    function userLoggedIn(user) {
        console.log("logged in");
        if (typeof user === "string") {
            userData = Backendless.Data.of(Backendless.User).findById(user);
        } else {
            userData = user;
        }
    }
    var userId = Backendless.LocalCache.get("current-user-id");
    var dataQuery = {condition: "ownerId = '" + userId + "'"};
    var tasksCollection = Backendless.Persistence.of(Posts).find(dataQuery);
       
    var wrapper = {
        posts: tasksCollection.data
    };
    Handlebars.registerHelper('format', function (time) {
        return moment(time).format("ddd, MMM Do YYYY");
    });

   
    var taskScript = $("#tasks-template").html();
    var taskTemplate = Handlebars.compile(taskScript);
    var taskHTML = taskTemplate(wrapper);

    $('.main-container').html(taskHTML);
    
    $(document).on('click', '.white-out-task', function(){
        var checkListScript = $("#check-done-template").html();
        var checkListTemplate  = Handlebars.compile(checkListScript);
        $('.main-container').html(checkListTemplate);
        console.log("complete task")
    });
    
    $(document).on('click', '.white-in-post', function(){
        var uncheckScript = $("#tasks-template").html(); 
        var uncheckTemplate = Handlebars.compile(uncheckScript);
        $('.main-container').html(uncheckTemplate);
    });


    function Posts(args) {
        args = args || {};
        this.content = args.content || "";
        this.authorName = args.authorName || "";
        this.complete = args.complete || "";
    }
    $(document).on('click', '.doneTask', function (event) {
               
        var dataStore = Backendless.Persistence.of(Posts)
        
        var markComplete = Backendless.Persistence.of(Posts).findById(event.target.attributes.data.nodeValue);
        
        markComplete["complete"] = !markComplete["complete"];
        
        dataStore.save(markComplete);
        Materialize.toast('DONE', 2000);
        location.reload();
            });
    $(document).on('click', '.deleteTask', function (event) {
        console.log(event.target.attributes.data.nodeValue);
        Backendless.Persistence.of(Posts).remove(event.target.attributes.data.nodeValue);
        Materialize.toast('DELETED', 2000);
        location.reload();
    });
});